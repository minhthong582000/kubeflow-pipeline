A commit to the main branch will trigger a build in Jenkins. The build script will execute the following steps:

1. Build the containers
2. Compile the KFP pipeline definition
3. Upload the new pipeline to KFP
4. Trigger a run of the new pipeline to train the model (this step is optional, depending on what makes sense for each model and the team’s workflow)
